-- | This module defines how to turn
--   the game state into a picture
module View where

import Graphics.Gloss
import Model

view :: GameState -> IO Picture
view = return . viewPure

viewPure :: GameState -> Picture
viewPure gstate = case infoToShow gstate of
  ShowNothing   -> blank
  ShowANumber n -> color green (text (show n))
  ShowAChar   c -> color green (text [c])
  ShowCircle  d -> color green (circle d)
  ShowField     -> tekenVeld gstate

{-
drawObject :: Posited a => a -> Picture
drawObject x = let pos = location x in
  case x of 
  Player  -> picturePlayer pos
  Crate   -> picture -}


getBricks :: GameState -> [Crate] -- dit is niet nodig, je kan de property bricks van gamestate aanroepen door: bricks gamestate. Dit is ook gedaan in de functie viewPure hierboven bij infoToShow gstate
getBricks GameState { bricks = cs } = cs

getPlayer :: GameState -> Player
getPlayer GameState { player = p } = p

-- deze kan beter, bv een functie die alles tekent
tekenVeld :: GameState -> Picture
tekenVeld gamestate  = pictures 
                      ([drawPicture F, drawPicture Wa, drawPicture (C (bricks gamestate)), drawPicture (B (bombs gamestate))] ++ [picturePlayer] )
                      ( pictureFields ++ pictureWalls ++ pictureTheCrates bricks ++ [picturePlayer player] )
                     where 
                     -- bricks = getBricks gamestate
                      bricks = map location (getBricks gamestate)
                      player = location (getPlayer gamestate)

fieldSize :: Float
fieldSize = 20

picturePositionate :: Pos -> Picture -> Picture
picturePositionate position = translate (fieldSize * fromIntegral (fst position)) ( fieldSize * fromIntegral (snd position)) 

getPicture :: DrawableObject -> Picture
getPicture F     = color green (line    [(0,0), (fieldSize, 0), (fieldSize,fieldSize), (0,fieldSize), (0,0)])
getPicture Wa    = color green (polygon [(0,0), (fieldSize,0), (fieldSize,fieldSize), (0,fieldSize)])
getPicture (C _) = color green (Pictures [Line [(0,0),(fieldSize,fieldSize)], Line [(0,fieldSize),(fieldSize,0)]])
getPicture (B _) = Polygon [(fieldSize / 4, 0), (0, fieldSize / 4), (fieldSize * 3 / 4, fieldSize), (fieldSize, fieldSize * 3 / 4)]

drawPicture :: DrawableObject -> Picture
drawPicture F        = pictures [picturePositionate (x, y) (getPicture F) | x <- [0 .. 10], y <- [0 .. 10], even x || even y]
drawPicture Wa       = pictures [picturePositionate (x, y) (getPicture Wa) | x <- [0 .. 10], y <- [0 .. 10], odd  x && odd  y]
drawPicture (C cs)   = pictures (map (flip picturePositionate (getPicture (C cs))) cs)
drawPicture (B bomb) = picturePositionate (bombposition (head bomb)) (color white (getPicture (B bomb)))














pictureField :: Picture
pictureField = color green (line [(0,0), (fieldSize, 0), (fieldSize,fieldSize), (0,fieldSize), (0,0)])

drawFields :: [Picture]
drawFields = [ picturePositionate (x, y) pictureField | x <- [0 .. 10], y <- [0 .. 10], even x || even y]          

getWallPicture :: Picture
getWallPicture = color green (polygon [(0,0),
                                    (fieldSize,0),
                                    (fieldSize,fieldSize), 
                                    (0,fieldSize)
                                   ])

drawWalls :: [Picture]
drawWalls = [ picturePositionate (x, y) getWallPicture | x <- [0 .. 10], y <- [0 .. 10], odd  x && odd y]


getCratePicture :: Picture
getCratePicture = color green (Pictures [Line [(0,0),(fieldSize,fieldSize)], Line [(0,fieldSize),(fieldSize,0)]])

pictureTheCrates :: [Pos] -> [Picture]
pictureTheCrates cs = map (picturePositionate pictureCrate) cs
drawCrates :: [Crate] -> [Picture]
drawCrates cs = map (flip picturePositionate getCratePicture) cs

picturePlayer :: Pos -> Picture
picturePlayer = picturePositionate playerPic
                      where 
                       bluecircle = color blue (thickCircle halfhalfs halfs)
                       halfs = fieldSize / 2
                       halfhalfs = fieldSize / 4
                        -- the circle needs translation before showing because of its weird origin
                       playerPic = translate halfs halfs bluecircle
