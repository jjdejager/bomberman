-- | This module defines how the state changes
--   in response to time and user input
module Controller where

import Model

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Random

eTime :: GameState -> Float
eTime GameState {elapsedTime = e} = e
-- | Handle one iteration of the game
step :: Float -> GameState -> IO GameState
step secs gstate
  {-| eTime gstate + secs > nO_SECS_BETWEEN_CYCLES
  = -- We show a new random number
    do gen <- getStdGen
       return (initialState gen)
  | otherwise
  = -- Just update the elapsed time
    do gen <- getStdGen
       return (initialState gen)

-- | Handle user input
input :: Event -> GameState -> IO GameState
input e gstate = return (inputKey e gstate)

inputKey :: Event -> GameState -> GameState
inputKey (EventKey (Char c) _ _ _) gstate
  | c == 'a'  = gstate { infoToShow = ShowCircle 100 }
  | c == 'b'  = gstate { infoToShow = ShowAChar 'b' }
  | otherwise = -- If the user presses a character key, show that one
    gstate { infoToShow = ShowAChar c }
inputKey _ gstate = gstate -- Otherwise keep the same