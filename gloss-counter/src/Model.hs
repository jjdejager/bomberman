-- | This module contains the data types
--   which represent the state of the game
module Model where

import System.Random
import Data.List
data InfoToShow = ShowNothing
                | ShowANumber Int
                | ShowAChar   Char
                | ShowCircle  Float
                | ShowField

data Crate           = Crate Pos
data GameResult      = InProgress GameState | Win | Loss
data Wall            = Wall Pos
data Bomb            = Bomb {
                             bombposition :: Pos
                            , range   :: Range
                            , time    :: Time
                            , player  :: Player
                            }
data Explosion       = Explosion Pos Time
type Pos             = (Int, Int)
type Time            = Float
type Range           = Int
data Dir             = N | E | S | W | O
data Vel             = N | E | S | W | O
data DrawableObject  = C [Crate] | Wa | B [Bomb] | Ex | P1 | P2 | F -- Crate Wall Bomb Explosion Player1 Player2 Field
--data ObjectsOnScreen = [(Pos, Object)]
data Player          = Player {
                                isComputer :: Bool
                                , position :: Pos
                                , velocity :: Dir
                                , mayPlaceBomb :: Bool
                                , bombAtPlace :: Bool
                              }
-- class to check that a datatype has a location
class Posited a where 
  location :: a -> Pos -- check where the object is located
  -- we know that every of those datatypes has the position directly hidden inside, 
  -- so we include a standard definition
  --location x {position = p} = p  
-- instances of class Posited : 
instance Posited Player where 
   location Player {position = p} = p
instance Posited Crate where 
   location (Crate p) = p
instance Posited Wall where 
   location (Wall p) = p
instance Posited Bomb where 
   location (Bomb p _ _ _) = p
instance Posited Explosion where 
   location (Explosion p _) = p  

nO_SECS_BETWEEN_CYCLES :: Float
nO_SECS_BETWEEN_CYCLES = 5


data GameState = GameState {
                    player :: Player,
--                    computer :: Player,
                    bricks  :: [Crate],
                    bombs   :: [Bomb],
--                    explosion :: [Explosion]
                    infoToShow :: InfoToShow,
                    elapsedTime :: Float
--                    g :: StdGen
                   }
initialState :: GameState
initialState = GameState startPlayer (Crate <$> [(0,0), (6,6)]) ShowField 0
                where 
                  startPlayer = Player False (0,0) O True False
initialState :: StdGen -> GameState
initialState a = GameState (generateCrates a) [Bomb (0, 10) 2 1 (Player True (1,1) O True True)] ShowField 0 

generateCrates :: StdGen -> [Crate] --creates the initial crates
generateCrates g = getPositions randomList
  where (g1,g2) = split g
        randomList     :: [Crate]
        randomList     = zip (randomRs (0,10) g1) (randomRs (0,10) g2)
        isStartingPlace :: Crate -> Bool
        isStartingPlace (a,b) | a == 0  && b > 7 = True
                              | a == 10 && b < 3 = True
                              | b == 0  && a > 7 = True
                              | b == 10 && a < 3 = True
                              | otherwise        = False  
        getPositions :: [Crate] -> [Crate]
        getPositions = filter (\b -> not (isStartingPlace b) && not (isWall b)) . map head . group . sort . take 200 

        


isWall :: Pos -> Bool
isWall (a, b) = odd a && odd b